import heat1D.mainprog as test
import sys
import numpy as np


sys.path.append('..\\heat1D')

data = test.read_input('..\\Examples\\Case_3\\Input_3.dat')

t_b = data.Value.Plate_temperature
t_inf = data.Value.Air_temperature
t_s = data.Value.Surrounding_temperature
l = data.Value.Fin_length
h = data.Value.Heat_transfer_coefficient
e = data.Value.Emissivity
k = data.Value.Thermal_conductivity
b_t = data.Value.Base_thickness
t_t = data.Value.Tip_thickness
n = int(data.Value.Number_finite_volumes)
res = data.Value.Residual

q_num = [730.5237781913547,
730.5958868743119,
730.6139167303627,
730.6184243689131]
m = np.sqrt(2*h/(k * b_t))
q_f = np.sqrt(2*h*k*b_t) * np.tanh(m*l) * (t_b-t_inf)

err = []
for q in q_num:
    err.append(abs(q-q_f)/q_f*100)
