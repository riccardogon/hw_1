import sympy as sym
import numpy as np


k = 50  # sym.Symbol('k')
A = sym.Symbol('A')
T_i = sym.Symbol('T_i')
T_m = sym.Symbol('T_m')
T_p = sym.Symbol('T_p')
T_inf = 10  # sym.Symbol('T_inf')
T_s = 10  # sym.Symbol('T_s')
dx = 0.002  # sym.Symbol('dx')
dAs = 2 * dx  # sym.Symbol('dAs')  # Constant for trapezoidal fins
h = 50  # sym.Symbol('h')
h_r = sym.Symbol('h_r')

dT1 = (T_i-T_m)/dx
dT2 = (T_p - T_i)/dx

gen_eq = sym.expand(sym.sympify((-k * A * dT1) + (k * A * dT2) - dAs * (h * (T_i - T_inf) + h_r * (T_i - T_s))))
# val = gen_eq.as_coefficients_dict()

arr = np.arange(0, 10)
test = []
for i, a in enumerate(arr):
    test.append(str(gen_eq.subs([(A, a), (T_m, 270), (T_p, 265), (h_r, 45)])))

