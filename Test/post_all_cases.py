import heat1D.post as post
import sys
import os


sys.path.append(r'C:\Users\richi\Documents\Git\HW_1\heat1D')

cases = [1, 2, 3]
n_fv = [10, 20, 40, 80]
base_dir = r'C:\Users\richi\Documents\Git\HW_1\Examples'
dir_str = r'\Case_'

for case in cases:
    res = []
    for n in n_fv:
        com_path = base_dir + dir_str + '{}\\{}\\'.format(case, n)
        os.chdir(com_path)
        post.post_process(com_path + 'Results_{}_{}.dat'.format(case, n), com_path + 'Input_{}_{}.dat'.format(case, n),
                          units='K', graph_type='all', save=True)

        # Reading the results for all cases
        with open('Results_{}_{}.dat'.format(case, n), 'r') as f:
            data = f.readlines()
        res.append(data[1][20:])

    with open(base_dir + dir_str + '{}\\Case_{}_results.dat'.format(case, case), 'w') as f:
        f.writelines(res)
