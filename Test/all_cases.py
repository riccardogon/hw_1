import heat1D.mainprog as h
import sys
import os
import shutil


sys.path.append(r'C:\Users\richi\Documents\Git\HW_1\heat1D')

cases = [1, 2, 3]
n_fv = [10, 20, 40, 80]
base_dir = r'C:\Users\richi\Documents\Git\HW_1\Examples'
dir_str = r'\Case_'

for case in cases:
    os.chdir(base_dir + dir_str + str(case))
    for n in n_fv:
        os.mkdir(str(n))
        dst = str(n) + '\\Input_{}_{}.dat'.format(case, n)
        shutil.copy(r'Input_{}.dat'.format(case), dst)
        with open(dst, 'r') as file:
            data = file.readlines()
        data[10] = 'Number_finite_volumes       {}                  /\n'.format(n)
        with open(dst, 'w') as file:
            file.writelines(data)

        h.heat1d(r'C:\Users\richi\Documents\Git\HW_1\Examples\Case_{}\{}\Input_{}_{}.dat'.format(case, n, case, n))
