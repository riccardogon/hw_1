# Heat1D

Heat1D is a python library that computes the heat flux per unit of width of a fin.

The Assumptions made in this script are: 

* Temperature variation is considered only in one direction. So the temperature variations within the fin cross section is considered negligible
* Steady state
* Costant thermal conductivity along the fin
* No thermal generation within the fin
* The fin surface is graydiffuse 
* The fluid is not participating nor emitting.

The python script *post.py* reads the input and output file from the *heat1D* calculations and plots the temperature distribution along the fin. Currently it outputs the images using latex so it is significantly slow.

**NOTE THAT ONLY CDS METHOD IS CURRENTLY AVAILABLE**

## Installation

Simply download **heat1D** folder containing *mainprog.py* and *post.py*

## Usage
Simply launch *mainprog.py* and it will ask the directory in which is present the input file. The heat flux will be computed and returned at screen. A output file containing the heat flux and temperature distribution along the fin will be also saved. Attention that there is no control on input units of measure, so use the units written in input.

For using it as a library:

```python
import heat1D.mainprog as h
import heat1D.post as post
import sys

sys.path.append('..\\heat1D')

h.heat1D(filepath='path_input_file', method='cds')

post.post_process(res_path='path_output', inp_path='path_input', units='C', graph_type='all', save=True)
```
Both *filepath* and *method* can be omitted. The code will ask for the directory of the input file and it will use the *cds* method.

## Input Format
Here's an example of an input file.

```bash
Variable                    Value               Units
Plate_temperature           400                 C
Air_temperature             -273.15             C
Surrounding_temperature     -273.15             C
Fin_length                  0.020               m
Heat_transfer_coefficient   50                  w/(m^2 K)
Emissivity                  0.7                 /
Thermal_conductivity        50                  w/(m K)
Base_thickness              0.001               m
Tip_thickness               0.001               m
Number_finite_volumes       10                  /
Residual                    1e-6                /

```

## Author
Riccardo Gon, [riccardo.gon@studenti.units.it](riccardo.gon@studenti.units.it)

Università degli Studi di Trieste,
Dipartimento di Ingegneria Navale

## License
[MIT](https://choosealicense.com/licenses/mit/)