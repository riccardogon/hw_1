"""
This script computes the heat flux per unit of width of a fin. The temperature variation is
considered only in one direction. So the temperature variations within the fin cross section is
considered negligible. Other assumptions are: Steady state, costant thermal conductivity along the fin,
no thermal generation within the fin, the fin surface is graydiffuse and the fluid is not participating
nor emitting.

For a example of the input file format see Input_example.dat in the Examples folder.
"""

import numpy as np
from scipy.sparse.linalg import spsolve
from scipy.sparse import spdiags
from pandas import read_csv
import os


def c2k(celsius):
    kelvin = celsius + 273.15
    return kelvin


def start(n):
    print(n*'-')
    print('|' + 'Starting heat1D'.center(n-2, ' ') + '|')
    print(n*'-')


def end(out_len, i, q):
    print('|' + (out_len-2)*' ' + '|')
    print('|' + 'Finished in {:} iterations'.format(i).center(out_len-2, ' ') + '|')
    print('|' + 'Computed heat flux: {:.2f}'.format(q).center(out_len-2, ' ') + '|')
    print('|' + (out_len-2)*' ' + '|')
    print(out_len * '-')


def read_input(filepath):
    input_data = read_csv(filepath, sep='\s+', index_col=0, usecols=[0, 1])
    return input_data


def define_dir(out_len):
    path = input('|' + 'Full path to the  directory: '.center(out_len-2) + '|\n')
    for file in os.listdir(path):
        if file[:6].lower() == 'input_':
            filepath = path + '\\' + file
            return filepath
    print('No input file found. Check if file starts with "input_"\nUppercase or lowercase')


def result_path(filepath):
    spl = filepath.lower().split('input_')
    file_out = spl[0] + 'Results_' + spl[1]
    return file_out


def fin_subdivision(dx, x_f, i_node, b_t, t_t, n):
    x_c = (x_f[i_node] + x_f[i_node + 1]) / 2       # CV centroids coordinates
    d_as = 2 * np.sqrt(abs(b_t - t_t)**2 + dx**2)   # External surface for each cv
    a_c = np.linspace(b_t, t_t, n+1)[1:]            # Cross section of the fin
    return x_c, d_as, a_c


def output(q, T, file_out):
    with open(file_out, 'w') as file:
        file.write('Results'.center(40, '-') + '\n')
        file.write('Heat flux'.ljust(20, ' ') + str(q) + '\n\n')
        file.write('Temperatures in K\n')
        for i in range(len(T)):
            file.write(str(T[i, 0])+'\n')


def heat1d(filepath=None, method='cds'):
    out_len = 80                                    # Output string length

    start(out_len)                                  # Starting screen output

    if filepath is None:
        filepath = define_dir(out_len=out_len)
    data = read_input(filepath=filepath)

    # Input variables and costants
    t_b = data.Value.Plate_temperature
    t_inf = data.Value.Air_temperature
    t_s = data.Value.Surrounding_temperature
    l = data.Value.Fin_length
    h = data.Value.Heat_transfer_coefficient
    e = data.Value.Emissivity
    k = data.Value.Thermal_conductivity
    b_t = data.Value.Base_thickness
    t_t = data.Value.Tip_thickness
    n = int(data.Value.Number_finite_volumes)
    res = data.Value.Residual

    sb = 5.670367e-8  # Stefan-Boltzman costant

    # Transforming temperatures in kelvin
    T_b = c2k(t_b)
    T_inf = c2k(t_inf)
    T_s = c2k(t_s)

    # Preallocating arrays
    A_e = np.zeros([n, 1], dtype=np.float64)    # East coefficients
    A_w = np.zeros_like(A_e)                    # West coefficients
    A_p = np.zeros_like(A_e)                    # Central coefficients
    S_p = np.zeros_like(A_e)                    # Source
    T = np.zeros_like(A_e)                      # Temperature

    # Discretization of the fin
    dx = l / n                                  # Length of each fv
    x_f = np.linspace(0, l, n + 1)              # Position of CV faces
    i_node = np.arange(0, n)                    # Node index
    x_c, d_as, a_c = fin_subdivision(dx=dx, x_f=x_f, i_node=i_node, b_t=b_t, t_t=t_t, n=n)

    if method == 'cds':
        from methods import cds as m
    else:
        print('Method not supported')

    # Equation coefficients that doesn't change
    A_e[:-1, 0] = m.east_coeff(k=k, dx=dx, a_c=a_c[:-1])
    A_w[1:, 0] = m.west_coeff(k=k, dx=dx, a_c=a_c[1:])

    # Iteration loop
    i = 0
    while 1:
        i += 1
        # h_r = 0
        h_r = e * sb * (T ** 2 + T_s ** 2) * (T + T_s)      # Radiation heat transfer coefficient
        A_p[:] = m.point_coeff(A_e=A_e, A_w=A_w, d_as=d_as, h=h, h_r=h_r)
        S_p[:] = m.source(d_as=d_as, h=h, T_inf=T_inf, h_r=h_r, T_s=T_s)

        # Boundary condition
        A_p[0], S_p[0] = m.boundary_cond(k=k, dx=dx, a_c=a_c, T_b=T_b, A=A_p, S=S_p)

        # Matrix creation
        A = spdiags([np.roll(A_w[:, 0], -1), A_p[:, 0], np.roll(A_e[:, 0], 1)], [-1, 0, 1], m=n, n=n, format='csr')

        # Equation resolution (T_new) and residual (diff)
        diff = T - (T_new := spsolve(A, S_p).reshape([n, 1]))

        # Check if the residual is less than wanted
        if abs(diff).max() < res:
            break
        else:
            T = T_new.copy()

    q = -2 * k * a_c[0] * (T_new[0, 0] - T_b) / dx

    end(out_len=out_len, i=i, q=q)
    file_out = result_path(filepath=filepath)
    output(q=q, T=T_new, file_out=file_out)


if __name__ == '__main__':
    heat1d(filepath=r'..\test\Input_test.dat', method='cds')
