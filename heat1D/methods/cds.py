def east_coeff(k, dx, a_c):
    A_e = (k / dx) * a_c
    return A_e


def west_coeff(k, dx, a_c):
    A_w = (k / dx) * a_c
    return A_w


def point_coeff(A_e, A_w, d_as, h, h_r):
    A_p = -(A_w + A_e) - d_as * (h + h_r)
    return A_p


def source(d_as, h, T_inf, h_r, T_s):
    S_p = -d_as * (h * T_inf + h_r * T_s)
    return S_p


def boundary_cond(k, dx, a_c, T_b, A, S):
    A_p = A[0] - (2 * k / dx) * a_c[0]
    S_p = S[0] - (2 * k / dx) * a_c[0] * T_b
    return A_p, S_p
