import matplotlib.pyplot as plt
from pandas import read_csv
import numpy as np
import matplotlib as mpl


def read_results(res_path):
    res = read_csv(res_path, skiprows=3)
    return res


def read_input(inp_path):
    data = read_csv(inp_path, sep='\s+', index_col=0, usecols=[0, 1])

    l = data.Value.Fin_length
    b_t = data.Value.Base_thickness
    t_t = data.Value.Tip_thickness
    n = int(data.Value.Number_finite_volumes)
    x_f = np.linspace(0, l, n + 1)
    i_node = np.arange(0, n)
    x_c = (x_f[i_node] + x_f[i_node + 1]) / 2
    a_c = np.linspace(b_t, t_t, n+1)

    t_b = data.Value.Plate_temperature
    t_inf = data.Value.Air_temperature
    t_s = data.Value.Surrounding_temperature
    max_temp = max(t_b, t_inf, t_s)
    min_temp = min(t_b, t_inf, t_s)

    return x_f, x_c, a_c, max_temp, min_temp, t_b


def post_process(res_path=None, inp_path=None, units='C', graph_type='graph', save=False):
    if res_path is None:
        res_path = input('Results file complete path: ')
    if inp_path is None:
        inp_path = input('Input file complete path: ')

    # Read the temperatures from the "Results_" file
    temp = read_results(res_path).to_numpy()
    x_f, x_c, a_c, max_t, min_t, t_b = read_input(inp_path)

    min_t = temp.min()

    # Changing the temperatures from K to C if needed
    if units == 'C':
        temp = temp - 273.15
        lab = '°C'
    elif units == 'K':
        lab = 'K'
        # min_t = min_t + 273.15
        max_t = max_t + 273.15
        t_b = t_b + 273.15
    else:
        print('Error in the units parameter')

    # Defining the max and min temperature for color scaling

    # Plot
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')

    if (graph_type == 'graph') or (graph_type == 'all'):
        fig2, ax3 = plt.subplots(1, 1)
        ax3.plot(x_c, temp)
        ax3.set_ylabel(r'\textbf{Temperature [K]}')
        ax3.set_xlabel(r'\textbf{Position on the fin [m]}')
        # ax3.set_title('Temperature distribution on the fin')
        ax3.grid(True)
        if not save:
            fig2.show()

    if (graph_type == 'color') or (graph_type == 'all'):

        fig, (ax, ax0) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [8, 1]}, figsize=(9, 5))

        # Upper part of base
        ax.plot((0, 0), ((a_c[0])*1.1, a_c[0]/2), color='k')
        # Lower part of base
        ax.plot((0, 0), (-(a_c[0])*1.1, -a_c[0]/2), color='k')
        # Fin tip
        ax.plot((x_f[-1], x_f[-1]), (a_c[0]/2, -a_c[0]/2), color='k')

        # Fin
        for i in range(len(temp)):
            ax.plot(x_f[i:i+2], a_c[i:i+2]/2, color='k')
            ax.plot(x_f[i:i+2], -a_c[i:i+2]/2, color='k')
            col = ((1/(max_t - min_t)*(temp[i, 0] - min_t)), 0., 1-(1/(max_t - min_t)*(temp[i, 0] - min_t)))
            ax.fill_between(x_f[i:i+2], a_c[i:i+2]/2, -a_c[i:i+2]/2, color=col)

        # Fill base
        base_col = ((1/(max_t - min_t)*(t_b-min_t)), 0., 1-(1/(max_t - min_t)*(t_b-min_t)))
        ax.fill_between(-x_f[0:2], (a_c[0]*1.1, a_c[0]*1.1), (-a_c[0]*1.1, -a_c[0]*1.1), color=base_col)

        # Temperature scale
        # Creating color map (from blue to red)
        norm = mpl.colors.Normalize(vmin=min_t, vmax=max_t)
        cdict = {'red': [(0.0, 0.0, 0.0),
                         (1.0, 1.0, 1.0)],
                 'green': [(0.0, 0.0, 0.0),
                           (1.0, 0.0, 0.0)],
                 'blue': [(0.0, 1.0, 1.0),
                          (1.0, 0.0, 0.0)]}
        test = mpl.colors.LinearSegmentedColormap('test', cdict)

        # ax.grid(True)
        # Plotting the temperature scale
        mpl.colorbar.ColorbarBase(ax0, cmap=test, norm=norm, orientation='horizontal')
        ax.axis('equal')
        ax.set_title('Temperature distribution on the fin.\nFin is considered monodimensional')
        ax.set_xlabel('[m]', verticalalignment='top')
        ax.set_ylabel('[m]')
        ax0.set_xlabel('Temperature [K]')
        fig.tight_layout()
        if not save:
            fig.show()

        if save:
            if (graph_type == 'graph') or (graph_type == 'all'):
                fig2.savefig('graph.pdf')
            if (graph_type == 'color') or (graph_type == 'all'):
                fig.savefig('color.pdf')


if __name__ == '__main__':
    post_process(res_path='..\\test\\Results_test.dat', inp_path='..\\test\\Input_test.dat', graph_type='graph',
                 units='K')
